var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var livereload = require('gulp-livereload');
var googleWebFonts = require('gulp-google-webfonts');



var plumberErrorHandler = {
    errorHandler: notify.onError({
        title: 'Gulp',
        message: 'Error: <%= error.message %>'
    })
};

var options = { };

gulp.task('fonts', function() {
   return gulp.src('./fonts.list')
        .pipe(plumber(plumberErrorHandler))
        .pipe(googleWebFonts(options))
        .pipe(gulp.dest('./fonts'));
});

gulp.task('font-awesome', function() {
  return gulp.src('node_modules/font-awesome/fonts/*')
      .pipe(gulp.dest('./fonts'))
})

gulp.task('sass', function () {
    gulp.src('./css/src/*.scss')
        .pipe(plumber(plumberErrorHandler))
        .pipe(sass())
        .pipe(gulp.dest('./css'))
        .pipe(livereload());
});

gulp.task('js', function () {
    gulp.src('js/src/*.js')
        .pipe(plumber(plumberErrorHandler))
        .pipe(jshint())
        .pipe(jshint.reporter('fail'))
        .pipe(concat('theme.js'))
        .pipe(gulp.dest('js'))
        .pipe(livereload());
});

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('css/src/*.scss', ['sass']);
    gulp.watch('js/src/*.js', ['js']);
});




gulp.task('default', ['sass', 'js', 'watch', 'fonts', 'font-awesome']);
